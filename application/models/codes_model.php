<?php 
/**
* 
*/
class Codes_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_codes()
	{
		$result =$this->db->select("*")
				->from("code_library")
				->get()
				->result_array();
		return $result;
	}
	public function add_codes()
	{
		$timestamp = date("dd:mm:yy G:i:s");
		$data = array(
			'language' => htmlentities($_POST["language"]),
			'framework' => htmlentities($_POST["framework"]),
			'cms' => htmlentities($_POST["cms"]),
			'title' => htmlentities($_POST["title"]),
			'description' => htmlentities($_POST["description"]),
			'code_text' => htmlentities($_POST["code_text"]),
			'added_by' => htmlentities($_POST["added_by"]),
			'timestamp' => $timestamp
			);
		$result = $this->db->insert("code_library",$data);
		return $result;
	}
}
	
?>