<?php 
/**
* 
*/
class Notes_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_notes()
	{
		$result =$this->db->select("*")
				->from("note_library")
				->get()
				->result_array();
		return $result;
	}
	public function add_notes()
	{
		$timestamp = date("dd:mm:yy G:i:s");
		$data = array(
			'language' => $_POST["language"],
			'framework' => $_POST["framework"],
			'cms' => $_POST["cms"],
			'title' => $_POST["title"],
			'description' => $_POST["description"],
			'added_by' => $_POST["added_by"],
			'timestamp' => $timestamp
			);
		$result = $this->db->insert("note_library",$data);
		return $result;
	}
}
	
?>