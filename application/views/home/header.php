<!DOCTYPE html>
<!-- 
Author : Sunarj Technologies
Website : www.sunarj.com
-->
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home | FreeCodeShare</title>
    <meta name="description" content="">
    <!-- View-port Basics: http://mzl.la/VYREaP -->
    <!-- This meta tag is used for mobile device to display the page without any zooming,
    so how much the device is able to fit on the screen is what's shown initially.
    Remove comments from this tag, when you want to apply media queries to the website. -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Normal favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <!-- PNG/GIF favicon -->
    <!-- Remove this block if not required -->
    <!-- <link rel="icon" type="image/gif" href="favicon.gif" />
    <link rel="icon" type="image/png" href="favicon.png" /> -->

    <!-- <link rel="stylesheet" href="bower_components/normalize.css/normalize.css"> -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  
     <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <script src="assets/js/jquery.min.js"></script>  
    <script src="assets/js/popper.min.js"></script>  
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Remove this if you intend to use modernizr-2.6.2 -->
    <!-- html5shiv aka html5 shim. Supporting HTML5 and CSS for IE browsers less than IE9. -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--Start of main container-->
    <div class="" style="padding:0;">
      <!--Start of wrapper container-->
      <div class="">
        <header class="">
            <h1>
              <a href="Home">
                <img src="assets/images/logo_sunarj.png" alt="Free Code Share" style="width:100px;height:100px;">
                FreeCodeShare                
              </a>              
            </h1>            
        </header>
        
        

        

        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
          <!-- Brand -->
          <a class="navbar-brand" href="#">FreeCodeShare</a>

          <!-- Toggler/collapsibe Button -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>

          <!-- Navbar links -->
          <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav  mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="#">Home</a>
              </li>
              <!-- Dropdown -->
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Languages
                </a>
                <div class="dropdown-menu">
                  <?php foreach ($languages as $key => $value): ?>
                  <a class="dropdown-item"  href="#"><?php echo $value["language_name"]; ?></a>
                  <?php endforeach ?>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="modal" data-target="#noteModal">Add Note</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="modal" data-target="#codeModal">Add Code</a>
              </li> 
            </ul>
            <ul class="navbar-nav">
              <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
          </div> 
        </nav>


