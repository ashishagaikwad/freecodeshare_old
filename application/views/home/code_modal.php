<!-- Note Modal Start -->
<script>
    // $(document).ready(function () {
      var clearmodal = function() {
        $("#codeModal input[type='text']").val("");
        $("#codeModal select").val("");
      }
    // });
    </script>
          <div id="codeModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

              <!-- Modal content-->
              <div class="modal-content">
                <form class ="note-add-form" action="<?php echo base_url('Codes/add_code'); ?>" method="POST" style="width:100%">
                <div class="modal-header">
                  <h4 class="modal-title">Add Code to Share</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12">
                      <span class="danger"><?php echo $this->session->flashdata("note_status"); ?></span>
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="language">Language</label>
                      <select class="col-md-6" name="language">
                        <option value="" selected disabled>Please Select</option>
                        <?php foreach ($languages as $key => $value) {
                          echo "<option value='".$value['language_name']."'>".$value['language_name']."</option>";
                        } ?>
                      </select>
                      <!-- <input class="col-md-6" type="text" name="language" id="language" placeholder="Please enter Language"> -->
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="framework">Framework</label>
                      <input class="col-md-6" type="text" name="framework" id="framework" placeholder="Please enter Framework">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="cms">CMS</label>
                      <input class="col-md-6" type="text" name="cms" id="cms" placeholder="Please enter CMS">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="title">Title</label>
                      <input class="col-md-6" type="text" name="title" placeholder="Please enter title" id="title">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="added_by">Your Name</label>
                      <input type="text" class="col-md-6" name="added_by" id="added_by" value="" placeholder="Enter Your Name" required>
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="description" style="vertical-align: top;">Description</label>
                      <textarea class="col-md-6" name="description" id="description" placeholder="Please add description or code"></textarea>
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="code_text" style="vertical-align: top;">Enter Your Code</label>
                      <textarea class="col-md-6" name="code_text" id="code_text" placeholder="Please add your code to save"></textarea>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-success btn-md" name="Submit" style="width: auto;">
                  <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" onclick="clearmodal()">Close</button>
                </div>
                </form>
              </div>

            </div>
          </div>
          <!-- Note Modal End -->