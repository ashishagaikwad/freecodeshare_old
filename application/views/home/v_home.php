        <!-- Jssor slider Required Script Start -->
        <?php if ($this->session->flashdata("note_status")) {
          echo "<script>JAlert('".$this->session->flashdata("note_status")."');</script>";
        } ?>
        <?php if ($this->session->flashdata("code_status")) {
          echo "<script>JAlert('".$this->session->flashdata("code_status")."');</script>";
        } ?>
        <script src="assets/js/jssor.slider-21.1.6.mini.js" type="text/javascript"></script>
        <script type="text/javascript">
          jQuery(document).ready(function ($) {
              
            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-1},{b:0,d:1000,o:1}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:1000,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:1900,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
          });          
        </script>
        <style>
          /* jssor slider bullet navigator skin 05 css */
          /*
          .jssorb05 div           (normal)
          .jssorb05 div:hover     (normal mouseover)
          .jssorb05 .av           (active)
          .jssorb05 .av:hover     (active mouseover)
          .jssorb05 .dn           (mousedown)
          */
          .jssorb05 {
              position: absolute;
          }
          .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
              position: absolute;
              /* size of bullet elment */
              width: 16px;
              height: 16px;
              background: url('<?php echo base_url("assets/images/slider/b05.png"); ?>') no-repeat;
              overflow: hidden;
              cursor: pointer;
          }
          .jssorb05 div { background-position: -7px -7px; }
          .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
          .jssorb05 .av { background-position: -67px -7px; }
          .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
          
          /* jssor slider arrow navigator skin 22 css */
          /*
          .jssora22l                  (normal)
          .jssora22r                  (normal)
          .jssora22l:hover            (normal mouseover)
          .jssora22r:hover            (normal mouseover)
          .jssora22l.jssora22ldn      (mousedown)
          .jssora22r.jssora22rdn      (mousedown)
          */
          .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('<?php echo base_url("assets/images/slider/a22.png"); ?>') center center no-repeat;
            overflow: hidden;
          }
          .jssora22l { background-position: -10px -31px; }
          .jssora22r { background-position: -70px -31px; }
          .jssora22l:hover { background-position: -130px -31px; }
          .jssora22r:hover { background-position: -190px -31px; }
          .jssora22l.jssora22ldn { background-position: -250px -31px; }
          .jssora22r.jssora22rdn { background-position: -310px -31px; }
        </style>
        <!-- Jssor slider Required Script End -->

        <section>
          <!-- <h2>Welcome to FreeCodeShare</h2> -->
          

          <!-- Jssor Slider Start -->
          <div class="row" style="margin:0;">
          <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
              <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
              <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
              <div style="position:absolute;display:block;background:url(<?php echo base_url("assets/images/slider/loading.gif"); ?>) no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
              <a data-u="any" href="http://www.jssor.com" style="display:none">Full Width Slider</a>
              <div data-p="225.00" style="display: none;">
                <img data-u="image" src="<?php echo base_url("assets/images/slider/red.jpg"); ?>" />
                <div style="position: absolute; top: 30px; left: 30px; width: 580px; height: 120px; font-size: 40px; color: #ffffff; line-height: 60px;">Welcome to FreeCodeShare</div>
                <div style="position: absolute; top: 100px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;"> 1. Add and Share Your Code<br>2. Search For Code<br>3. Help Others to Grow. </div>
                <!-- <div data-u="caption" data-t="0" style="position: absolute; top: 120px; left: 650px; width: 470px; height: 220px;">
                  <img style="position: absolute; top: 0px; left: 0px; width: 470px; height: 220px;" src="<?php echo base_url("assets/images/slider/c-phone-horizontal.png"); ?>" />
                  <div style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px; overflow: hidden;">
                    <img data-u="caption" data-t="1" style="position: absolute; top: 0px; left: 0px; width: 379px; height: 213px;" src="<?php echo base_url("assets/images/slider/c-slide-1.jpg"); ?>" />
                    <img data-u="caption" data-t="2" style="position: absolute; top: 0px; left: 379px; width: 379px; height: 213px;" src="<?php echo base_url("assets/images/slider/c-slide-3.jpg"); ?>" />
                  </div>
                  <img style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px;" src="<?php echo base_url("assets/images/slider/c-navigator-horizontal.png"); ?>" />
                  <img data-u="caption" data-t="3" style="position: absolute; top: 740px; left: 1600px; width: 257px; height: 300px;" src="<?php echo base_url("assets/images/slider/c-finger-pointing.png"); ?>" />
                </div> -->
              </div>
              <div data-p="225.00" style="display: none;">
                <img data-u="image" src="<?php echo base_url("assets/images/slider/purple.jpg"); ?>" />
                <div style="position: absolute; top: 30px; left: 30px; width: 580px; height: 120px; font-size: 40px; color: #ffffff; line-height: 60px;">Why we do this?</div>
                <div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Because We love to Share</div>
              </div>
              <div data-p="225.00" data-po="80% 55%" style="display: none;">
                <img data-u="image" src="<?php echo base_url("assets/images/slider/blue.jpg"); ?>" />
              </div>
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
              <!-- bullet navigator item prototype -->
              <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
          </div>

          </div>
          <!-- #endregion Jssor Slider End -->
          <!-- Jssor Slider End -->
          
          <div style="overflow:auto;">
            <br><br>
            <h3>Recently Added Notes</h3>
            <table  class="table">
            <!-- Here we search notes -->
              <tr style="font-weight:800;border-top:2px solid gray;border-bottom:2px solid gray;">
                <th>Title</th>
                <th>Description</th>
                <th>Lang</th>
                <th>Framework</th>
                <th>CMS</th>
                <th>Added by</th>
              </tr>
            <?php /*print_r($notes);*/foreach ($notes as $key => $value) {
            ?>
            <tr>
              <td><?php echo $value["title"]; ?></td>
              <td><?php echo $value["description"]; ?></td>
              <td><?php echo $value["language"]; ?></td>
              <td><?php echo $value["framework"]; ?></td>
              <td><?php echo $value["cms"]; ?></td>
              <td><?php echo $value["added_by"]; ?></td>
            </tr> 
            <?php
            } ?>
            </table>
          </div>
          <div class="col-md-12" style="overflow:auto;margin: 0;padding: 0;">
            <br>
            <h3>Recently Added Codes</h3>
            <table  class="table">
            <!-- Here we search notes -->
              <tr style="font-weight:800;border-top:2px solid gray;border-bottom:2px solid gray;">
                <th>Title</th>
                <th>Description</th>
                <th>Lang</th>
                <th>Framework</th>
                <th>CMS</th>
                <th>Code</th>
                <th>Added by</th>
              </tr>
            <?php /*print_r($notes);*/foreach ($codes as $key => $value) {
            ?>
            <tr>
              <td><?php echo $value["title"]; ?></td>
              <td><?php echo $value["description"]; ?></td>
              <td><?php echo $value["language"]; ?></td>
              <td><?php echo $value["framework"]; ?></td>
              <td><?php echo $value["cms"]; ?></td>
              <td><?php echo $value["code_text"]; ?></td>
              <td><?php echo $value["added_by"]; ?></td>
            </tr> 
            <?php
            } ?>
            </table>
          </div>
        </section>
        <aside></aside>
        <!-- Javascript Section Start -->

        <!-- Javascript Section End -->
        
