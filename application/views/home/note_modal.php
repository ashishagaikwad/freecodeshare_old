<!-- Note Modal Start -->
          <div id="noteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form class ="note-add-form" action="<?php echo base_url('Notes/add_note'); ?>" method="POST" style="width:100%">
                <div class="modal-header">
                  <h4 class="modal-title">Add Note</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12">
                      <span class="danger"><?php echo $this->session->flashdata("note_status"); ?></span>
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="language">Language</label>
                      <input class="col-md-6" type="text" name="language" id="language">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="framework">Framework</label>
                      <input class="col-md-6" type="text" name="framework" id="framework">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="cms">CMS</label>
                      <input class="col-md-6" type="text" name="cms" id="cms">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="title">Title</label>
                      <input class="col-md-6" type="text" name="title" id="title">
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="description" style="vertical-align: top;">Description and Code</label>
                      <textarea class="col-md-6" name="description" id="description" title="Please add description or code"></textarea>
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-5" for="added_by">Your Name</label>
                      <input type="text" class="col-md-6" name="added_by" id="added_by" value="guest">
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-success btn-md" name="Submit" style="width: auto;">
                  <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">Close</button>
                </div>
                </form>
              </div>

            </div>
          </div>
          <!-- Note Modal End -->