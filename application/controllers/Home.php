<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');// required for using flashdata
		$this->load->helper('url');// required for using base_url
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$this->load->model("notes_model");
		$this->load->model("codes_model");
		$this->load->model("home_model");
		$output["languages"] = $this->home_model->get_languages();
		$output["notes"] = $this->notes_model->get_notes();
		$output["codes"] = $this->codes_model->get_codes();
		$output["content"] = "v_home";
		$this->output_to_view('home_template',$output);
	}

	public function output_to_view($view='',$output='')
	{
		$this->load->view($view,$output);
	}
}
