<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends CI_Controller {

	/**
	 * This is the controller to add Notes
	 * Author : Ashish Arjun Gaikwad
	 * Copyright : Sunarj Technologies
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function add_note()
	{	
		$this->load->model("notes_model");

		$this->session->set_flashdata(array("note_status"=>$this->notes_model->add_notes()));
		redirect("Home");
		/*$output["content"] = "v_home";
		$this->output_to_view('home_template',$output);*/
	}

	public function output_to_view($view='',$output='')
	{
		$this->load->view($view,$output);
	}
}
