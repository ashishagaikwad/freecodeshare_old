var gulp = require('gulp');
var browserSync = require('browser-sync').create();// create a browser sync instance.
var reload      = browserSync.reload;
var htmlmin = require('gulp-htmlmin');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
//Variable declarations
var environment = process.env.NODE_ENV || 'freecodeshareelopment',
    directoryName = __dirname,
    folderName,
    portNumber;
//-------------------------------
//htmlv = require('gulp-html-validator');

// Default
/*gulp.task('valid', function () {
  gulp.src('./*.html')
    .pipe(htmlv())
    .pipe(gulp.dest('./prod'));
});
*/
// Option format set to html
/*gulp.task('invalid', function () {
  gulp.src('*.html')
    .pipe(htmlv({format: 'html'}))
    .pipe(gulp.dest('prod'));
});*/

/*var htmlhint = require("gulp-htmlhint");
 
gulp.task('validate-html', function() {
  return gulp.src("./freecodeshare/*.html")
    .pipe(htmlhint())
    .pipe(htmlhint.reporter())
    .pipe(gulp.dest('prod/'))
}); */
//-------------------------------    
 
gulp.task('minify-html', function() {
  return gulp.src('freecodeshare/application/views/*.php')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('prod'))
});

gulp.task('minify-css', function () {
  return  gulp.src('freecodeshare/assets/css/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('prod/assets/css'));
});

gulp.task('browser-sync', function() {
    /*if (environment == 'freecodeshareelopment') {
      folderName = 'application';
      portNumber = 80;
    } else if (environment == 'production') {
      folderName = 'application';
      portNumber = 80;
    }*/
    // Show me additional info about the process
    
    
    logLevel: "info";
    logLevel: "debug";
    logConnections: true;
    logSnippet: true;

    browserSync.init({

        port: 80,
        http: true,
        open: "local",
        browser: ["chrome", "firefox", "iexplore"],
        files: ['./*.html','./*.css'],
        reloadDelay: 500,
        
        server: {
          baseDir: "freecodeshare",
          index: "index.html"
        }

    });
    
});

/*gulp.task('browser-sync', function() {
  if (environment == 'freecodeshareelopment') {
    folderName = 'freecodeshare';
    portNumber = 80;
  } else if (environment == 'production') {
    folderName = 'prod';
    portNumber = 80;
  }

  var projectName = directoryName.split('\\'),
      projName = projectName[projectName.length - 1];

  browserSync({

    browser: ["chrome", "firefox", "iexplore"],

    files: [folderName+'*.html', folderName+'assets/css/*.css', folderName+'assets/js/*.js'],

    ghostMode: {
      clicks: true,
      forms: true,
      scroll: true
    },

    logFileChanges: true,

    logPrefix: projName,

    port: portNumber,

    server: {
      baseDir: [folderName],
      index: "a.html"
    }
  });
});*/


gulp.task('default',['browser-sync','minify-html','minify-css'], function() {
  // place code for your default task here
  /*console.log('default!!!');
  gulp.watch('freecodeshare/*.html', [browserSync.reload]);
  console.log('default!!!end');*/
  //gulp.watch('freecodeshare/*.html',browserSync.reload);
  //gulp.watch("/*.html").on("change", reload);
  gulp.watch('freecodeshare/application/views/*.php', ['minify-html',browserSync.reload]);
  gulp.watch('freecodeshare/assets/css/*.css', ['minify-css',browserSync.reload]);
  gulp.watch('freecodeshare/assets/js/*.js', browserSync.reload);
});
